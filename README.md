# Shibboleth Cookie Servlet Package

* [Terminating IdP Session](#Logout)
* [X509 Cookie Management](#X509)
* [Installation](#Installation)

## <a name="Logout">Terminating IdP Session</a>

`idp.jsp` attempts to destroy the IdP session (i.e. log out of the IdP) by returning an expired IdP session cookie (`shib_idp_session`). For completeness, it also changes the value of the session cookie to `__terminated__`. The default location for this endpoint is `https://IDP.example.edu/cookie/idp`.

## <a name="X509">X509 Cookie Management</a>

We use the `x509passthrough` cookie to trigger X.509 client certificate authentication. The default Shibboleth X509 login UI checks for the presence of this cookie, but does not check its value. Our custom `firstFactor.js` handler does check the value, and it must be a String with the value `1` or `true`.

When `x509.jsp` is called with no parameters, or with `?action=enable`, it will create an `x509passthrough` cookie with the value `1`, path set to `/`, and expiration set to 5 years (the default validity of our client certificates). When called with `?action=disable`, it returns an expired `x509passthrough` cookie with the value `0`. Normally the endpoint is `https://IDP.example.edu/cookie/x509`.

## <a name="Installation">Installation</a>

The [WAR](https://repo.itlab.stanford.edu/maven/release/edu/stanford/itlab/shibboleth/CookieServlet/1.0.4/CookieServlet-1.0.4.war), its GPG [signature](https://repo.itlab.stanford.edu/maven/release/edu/stanford/itlab/shibboleth/CookieServlet/1.0.4/CookieServlet-1.0.4.war.asc), and its SHA1 [hash](https://repo.itlab.stanford.edu/maven/release/edu/stanford/itlab/shibboleth/CookieServlet/1.0.4/CookieServlet-1.0.4.war.sha1) can be downloaded from the IT Lab [repo](http://repo.itlab.stanford.edu/).

Download the IT Lab Repo signing key:

```shell
% gpg --receive-keys 9A559486A1F8A1DA93809EA37CBDD8B10F39DA6C
gpg: key 7CBDD8B10F39DA6C: public key "Stanford IT Lab Repository <admin@itlab.stanford.edu>" imported
gpg: no ultimately trusted keys found
gpg: Total number processed: 1
gpg:               imported: 1
```

then verify the signature on the JAR file:

```shell
% gpg --verify idp-support-1.0.jar.asc idp-support-1.0.jar
gpg: Signature made Wed Mar 14 09:06:01 2018 PDT
gpg:                using RSA key 9A559486A1F8A1DA93809EA37CBDD8B10F39DA6C
gpg: Good signature from "Stanford IT Lab Repository <admin@itlab.stanford.edu>" [unknown]
...
```

### Build and Install the WAR file

Alternatively, you can clone this repo and build the WAR:

```shell
% mvn clean package
```

This will create `CookieServlet-1.0.4.jar` in the `target`
directory. Copy this file to the IdP (normally under
`/opt/cookieservlet`), then modify the Tomcat configuration.

### Tomcat Configuration

The servlet can be added to any existing `Host` element in `server.xml`:

```xml
<Context path="/cookie"
         docBase="/opt/cookieservlet/CookieServlet-1.0.4.war"
         privileged="true"
         antiResourceLocking="false"
         unpackWAR="false"
         swallowOutput="true" />
```

