<%@ page import="javax.servlet.http.Cookie" %>
<%
/**
 * Simplified X509 Cookie Setter
 */

final String COOKIE_NAME     = "x509passthrough";
final String COOKIE_ENABLED  = "1";
final String COOKIE_DISABLED = "0";

final String   location     = "/cookie/x509";
final Cookie[] cookies      = request.getCookies();
      Cookie   c1           = new Cookie(COOKIE_NAME, ""),
               c2           = null;
      String   action       = request.getParameter("action"),
               new_value    = null,
               old_value    = null,
               error        = null;
      int      max_age      = -1;

if (cookies != null) {
  for (final Cookie cookie : cookies) {
    if (cookie.getName().equals(COOKIE_NAME)) {
      old_value = cookie.getValue();
    }
  }
}

if (action == null) {
  action = "";
}

if (old_value == null || (!old_value.equals(COOKIE_ENABLED) && !old_value.equals(COOKIE_DISABLED))) {
  old_value = COOKIE_DISABLED;
}


switch (action) {
  case "enable":
    // enable cert authentication
    new_value = COOKIE_ENABLED;
    break;

  case "disable":
    // disable cert authentication
    new_value = COOKIE_DISABLED;
    break;

  case "check":
  case "":
    // don't change the current setting
    new_value = old_value;
    break;

  default:
    error = "Invalid action";
    break;
}

if (error == null && !new_value.equals(old_value)) {
  c1.setPath("/");
  c1.setSecure(true);
  c1.setHttpOnly(false);
  c1.setComment("Track certificate authentication preference");
  // enable cookie for 5 years, or instruct user-agent to delete cookie
  c1.setMaxAge(new_value == COOKIE_ENABLED ? 5 * 365 * 86400 : 0);
  c1.setValue(new_value);
  response.addCookie(c1);
}

%>
<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cardinal Key</title>
    <link href="<%=request.getContextPath()%>/css/certificate.css" rel="stylesheet">
  </head>

  <body>

    <div class="box">
      <p>
        Cardinal Keys reduce or eliminate the need to use your SUNet
        ID and password for web-based logins.
      </p>

      <p>
        You need to install a Cardinal Key on every device and
        enable it on every browser you plan to use with Cardinal Key
        authentication. For more information, visit <a target="_blank"
        href="https://cardinalkey.stanford.edu/">cardinalkey.stanford.edu</a>.
      </p>
    </div>

  <% if (error != null) { %>
    <div class="box box-danger">
      <h2>Error!</h2>
      <p><%= error %></p>
    </div>
  <% } else if (new_value.equals(COOKIE_ENABLED)) { %>
    <div class="box box-success">
      <h2>Cardinal Key Enabled</h2>
      <p>Cardinal Key authentication is enabled for this browser.</p>
    </div>
    <div class="action">
      <p><a href="?action=disable" class="btn btn-danger">Disable Cardinal Key Authentication</a></p>
    </div>
  <% } else { %>
    <div class="box box-danger">
      <h2>Cardinal Key Disabled</h2>
      <p>Cardinal Key authentication is disabled for this browser.</p>
    </div>
    <div class="action">
      <p><a href="?action=enable" class="btn btn-success">Enable Cardinal Key Authentication</a></p>
    </div>
  <% } %>
  </body>
</html>
