<%@ page import="javax.servlet.http.Cookie" %>
<%
/**
 * Simplified IdP Cookie Clearer
 */

final Cookie c1 = new Cookie("shib_idp_session", "__terminated__"),
             c2;

c1.setPath("/");
c1.setSecure(true);
c1.setMaxAge(0);
c2 = (Cookie)c1.clone();
c2.setPath("/idp");

response.addCookie(c1);
response.addCookie(c2);

%>
<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IdP Session</title>
    <link href="<%=request.getContextPath()%>/css/certificate.css" rel="stylesheet">
  </head>

  <body>
    <div class="box box-warning">
      <h2>Session Terminated</h2>
      <p>Your session with this IdP has been terminated.</p>
    </div>
  </body>
</html>
